output "azurerm_public_ip_consul" {
  description = "The public IP address of the Linux VM"
  value       = azurerm_public_ip.consul-pip.ip_address
}
