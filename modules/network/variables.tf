# Declare any spcific variables needed for network infrastructure
variable "rg_name" {
  description = "Resource group to use"
}

variable "location" {
  description = "Region where the network components will be deployed"
}

variable "net_name" {
  description = "Name to be used on network resources"
}
variable "cidr" {
  description = "CIDR range to deploy/use for network"
}

variable "subnet_prefix" {
  description = "Address prefixes to use for subnet"
}

variable "tag_environment" {
  description = "Tag for the environment"
}

variable "tag_ttl" {
  description = "Tag for TTL of the environment"
}

variable "tag_owner" {
  description = "Tag for owner of the environment"
}
