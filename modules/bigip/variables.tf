variable "rg_name" {
  description = "Resource group to use"
}

variable "location" {
  description = "Region where the network components will be deployed"
}

variable "subnet_id" {
  description = "The subnet required"
}

variable "netsec_group_id" {
  description = "The network security group id"
}

variable "instance_type" {
  description = "Type of VM - eg. Standard_A1_v2"
}

variable "admin_username" {
  description = "Set the default administrator username"
  default     = "f5admin"
}

variable "publisher" {
  description = "Publisher of the BigIP on Azure Marketplace"
}

variable "product" {
  description = "Product name"
}

variable "image_name" {
  description = "Image name to use for BigIP"
}

variable "bigip_version" {
  description = "BigIP version to use"
}

variable "tag_name" {
  description = "Tag name of the bigip"
}

variable "tag_environment" {
  description = "Tag name of the environment"
}

variable "tag_workload" {
  description = "Tag name of workload"
}

variable DO_URL {
  description = "URL to download the BIG-IP Declarative Onboarding module"
  type        = string
  default     = "https://github.com/F5Networks/f5-declarative-onboarding/releases/download/v1.13.0/f5-declarative-onboarding-1.13.0-5.noarch.rpm"
}

## Please check and update the latest AS3 URL from https://github.com/F5Networks/f5-appsvcs-extension/releases/latest 
# always point to a specific version in order to avoid inadvertent configuration inconsistency
variable AS3_URL {
  description = "URL to download the BIG-IP Application Service Extension 3 (AS3) module"
  type        = string
  default     = "https://github.com/F5Networks/f5-appsvcs-extension/releases/download/v3.20.0/f5-appsvcs-3.20.0-3.noarch.rpm"
}

## Please check and update the latest TS URL from https://github.com/F5Networks/f5-telemetry-streaming/releases/latest 
# always point to a specific version in order to avoid inadvertent configuration inconsistency
variable TS_URL {
  description = "URL to download the BIG-IP Telemetry Streaming Extension (TS) module"
  type        = string
  default     = "https://github.com/F5Networks/f5-telemetry-streaming/releases/download/v1.12.0/f5-telemetry-1.12.0-3.noarch.rpm"
}
variable "libs_dir" {
  description = "Directory on the BIG-IP to download the A&O Toolchain into"
  type        = string
  default     = "/config/cloud/aws/node_modules"
}

variable onboard_log {
  description = "Directory on the BIG-IP to store the cloud-init logs"
  type        = string
  default     = "/var/log/startup-script.log"
}
