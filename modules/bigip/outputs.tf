output "random_password_generated" {
  description = "A randomly generated password"
  value       = random_password.bigippassword.result
}

output "linux_vm_ext_nic_privateip" {
  description = "The private IP address of the Linux VM"
  value       = azurerm_network_interface.ext-nic.private_ip_address
}

output "azurerm_public_ip_bigipvm" {
  description = "The public IP address of the Linux VM"
  value       = azurerm_public_ip.sip_public_ip.ip_address
}


