# Declare any spcific variables needed for security infrastructure
variable "rg_name" {
  description = "Resource group to use"
}

variable "location" {
  description = "Region where the network components will be deployed"
}

variable "f5_secgrp_name" {
  description = "Name of the f5 bigip security group"
}

variable "consul_secgrp_name" {
  description = "Name of the consul security group"
}

variable "websvr_secgrp_name" {
  description = "Name of the webserver security group"
}

