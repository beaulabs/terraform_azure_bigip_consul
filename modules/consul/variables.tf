# Declare any spcific variables needed for security infrastructure
variable "rg_name" {
  description = "Resource group to use"
}

variable "location" {
  description = "Region where the network components will be deployed"
}

variable "netsec_group_id" {
  description = "The network security group id to use"
}

variable "admin_username" {
  description = "Set the default administrator username"
  default     = "f5admin"
}

variable "admin_password" {
  description = "Set the default administrator password"
}

variable "name_prefix" {
  description = "Name to prefix consul resources with"
}

variable "consul_subnet_id" {
  description = "Consul subnet id to use"
}

variable "priv_ip" {
  description = "Private IP address for consul"
}
