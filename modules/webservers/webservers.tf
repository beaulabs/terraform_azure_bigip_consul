
resource "azurerm_virtual_machine_scale_set" "vmss" {
  name                = var.scaleset_name
  resource_group_name = var.rg_name
  location            = var.location
  upgrade_policy_mode = "Manual"

  sku {
    name     = var.instance_type
    tier     = "Standard"
    capacity = var.scaleset_capacity
  }

  storage_profile_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_profile_os_disk {
    name              = ""
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  storage_profile_data_disk {
    lun           = 0
    caching       = "ReadWrite"
    create_option = "Empty"
    disk_size_gb  = 10
  }

  os_profile {
    computer_name_prefix = var.vm_name_prefix
    admin_username       = var.admin_username
    admin_password       = var.admin_password
    #custom_data          = base64encode(file("./templates/webserver.sh"))
    custom_data = file("${path.root}/templates/webserver.sh")
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  network_profile {
    name                      = var.net_profile
    primary                   = true
    network_security_group_id = var.net_secgrp
    ip_configuration {
      name      = "IPConfiguration"
      subnet_id = var.subnet_id
      primary   = true
    }
  }
}
