# Create virtual network
resource "azurerm_virtual_network" "main" {
  name                = "${var.net_name}-vnet"
  address_space       = [var.cidr]
  location            = var.location
  resource_group_name = var.rg_name

  tags = {
    environment = var.tag_environment
    TTL         = var.tag_ttl
    Owner       = var.tag_owner
  }
}

# Create public/external subnet
resource "azurerm_subnet" "public" {
  name = "${var.net_name}-public"
  # resource_group_name  = azurerm_resource_group.main.name
  resource_group_name  = var.rg_name
  virtual_network_name = azurerm_virtual_network.main.name
  address_prefixes     = [var.subnet_prefix]
}
