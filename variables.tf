variable "region" {
  description = "Azure region to deploy into"
  default     = "centralus"
}

variable "prefix" {
  description = "Prefix to use for resources if requested"
  #default = "f5-azure-consul"
  default = "beau-f5-consul"
}

variable "owner" {
  description = "Owner of resources running"
  default     = "beau"
}

variable "environment" {
  description = "Type of environment"
  default     = "test"
}

variable "default_admin" {
  description = "Default admin for this lab"
  default     = "f5admin"
}

variable image_name {
  description = "VM image to use"
  default     = "f5-bigip-virtual-edition-25m-best-hourly"
}

variable publisher {
  description = "Publisher of VM image"
  default     = "f5-networks"
}

variable product {
  description = "Used in offer of the VM image"
  default     = "f5-big-ip-best"
}

variable bigip_version {
  description = "Version of VM image"
  default     = "14.1.003000"
}


variable DO_URL {
  description = "URL to download the BIG-IP Declarative Onboarding module"
  type        = string
  default     = "https://github.com/F5Networks/f5-declarative-onboarding/releases/download/v1.13.0/f5-declarative-onboarding-1.13.0-5.noarch.rpm"
}

## Please check and update the latest AS3 URL from https://github.com/F5Networks/f5-appsvcs-extension/releases/latest 
# always point to a specific version in order to avoid inadvertent configuration inconsistency
variable AS3_URL {
  description = "URL to download the BIG-IP Application Service Extension 3 (AS3) module"
  type        = string
  default     = "https://github.com/F5Networks/f5-appsvcs-extension/releases/download/v3.20.0/f5-appsvcs-3.20.0-3.noarch.rpm"
}

## Please check and update the latest TS URL from https://github.com/F5Networks/f5-telemetry-streaming/releases/latest 
# always point to a specific version in order to avoid inadvertent configuration inconsistency
variable TS_URL {
  description = "URL to download the BIG-IP Telemetry Streaming Extension (TS) module"
  type        = string
  default     = "https://github.com/F5Networks/f5-telemetry-streaming/releases/download/v1.12.0/f5-telemetry-1.12.0-3.noarch.rpm"
}
variable "libs_dir" {
  description = "Directory on the BIG-IP to download the A&O Toolchain into"
  type        = string
  default     = "/config/cloud/aws/node_modules"
}

variable onboard_log {
  description = "Directory on the BIG-IP to store the cloud-init logs"
  type        = string
  default     = "/var/log/startup-script.log"
}
