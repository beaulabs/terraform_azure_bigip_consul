provider "azurerm" {
  version = "2.13.0"
  features {}
}

locals {
  environment = "${var.prefix}-demo"
  TTL         = 24
  owner       = var.owner
}

# Create a resource group
resource "azurerm_resource_group" "main" {
  name     = "${var.prefix}-rg"
  location = var.region

  tags = {
    environment = local.environment
    TTL         = local.TTL
    Owner       = local.owner
  }
}

# Create the required network components
module "network" {
  source          = "./modules/network/"
  net_name        = var.prefix
  rg_name         = azurerm_resource_group.main.name
  location        = azurerm_resource_group.main.location
  cidr            = "10.0.0.0/8"
  subnet_prefix   = "10.0.0.0/24"
  tag_environment = local.environment
  tag_ttl         = local.TTL
  tag_owner       = local.owner
}

# Create security groups for the network components
module "security" {
  source             = "./modules/security/"
  f5_secgrp_name     = "f5-public-security-group"
  consul_secgrp_name = "consul-security-group"
  websvr_secgrp_name = "webserver-security-group"
  rg_name            = azurerm_resource_group.main.name
  location           = azurerm_resource_group.main.location
}

# Create required webservers
module "webservers" {
  source            = "./modules/webservers"
  rg_name           = azurerm_resource_group.main.name
  location          = azurerm_resource_group.main.location
  scaleset_name     = "vmscaleset"
  net_profile       = "tfnetworkprofile"
  subnet_id         = module.network.subnet_public_id
  net_secgrp        = module.security.webserver_security_group_id
  instance_type     = "Standard_A2_v2"
  scaleset_capacity = "2"
  admin_username    = var.default_admin
  admin_password    = module.bigip.random_password_generated
  vm_name_prefix    = "vmlab"
}

module "consul" {
  source           = "./modules/consul"
  rg_name          = azurerm_resource_group.main.name
  location         = azurerm_resource_group.main.location
  netsec_group_id  = module.security.consul_security_group_id
  name_prefix      = var.prefix
  consul_subnet_id = module.network.subnet_public_id
  priv_ip          = "10.0.0.100"
  admin_password   = module.bigip.random_password_generated
}

module "bigip" {
  source          = "./modules/bigip"
  rg_name         = azurerm_resource_group.main.name
  location        = azurerm_resource_group.main.location
  netsec_group_id = module.security.f5_public_security_group_id
  instance_type   = "Standard_DS3_v2"
  admin_username  = var.default_admin
  publisher       = "f5-networks"
  product         = "f5-big-ip-best"
  image_name      = "f5-bigip-virtual-edition-25m-best-hourly"
  #bigip_version   = "15.1.0.2-0.0.9"
  bigip_version   = "14.1.003000"
  tag_name        = "bigip"
  tag_environment = local.environment
  tag_workload    = "ltm"
  DO_URL          = "https://github.com/F5Networks/f5-declarative-onboarding/releases/download/v1.13.0/f5-declarative-onboarding-1.13.0-5.noarch.rpm"
  AS3_URL         = "https://github.com/F5Networks/f5-appsvcs-extension/releases/download/v3.20.0/f5-appsvcs-3.20.0-3.noarch.rpm"
  TS_URL          = "https://github.com/F5Networks/f5-telemetry-streaming/releases/download/v1.12.0/f5-telemetry-1.12.0-3.noarch.rpm"
  libs_dir        = "/config/cloud/aws/node_modules"
  onboard_log     = "/var/log/startup-script.log"
  subnet_id       = module.network.subnet_public_id


}

module "as3" {
  source            = "./modules/as3"
  vip_address       = module.bigip.linux_vm_ext_nic_privateip
  public_ip_address = module.bigip.azurerm_public_ip_bigipvm
  admin_password    = module.bigip.random_password_generated

  #Emulate a depends_on for modules
  dependson_linux_vm_f5bigip = [module.bigip]

}
