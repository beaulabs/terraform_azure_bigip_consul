# Declare any spcific variables needed for security infrastructure
variable "rg_name" {
  description = "Resource group to use"
}

variable "location" {
  description = "Region where the network components will be deployed"
}

variable "scaleset_name" {
  description = "Name of the virtual machine scale set"
}

variable "net_profile" {
  description = "Network profile for the virtual machine scale set"
}

variable "instance_type" {
  description = "Type of VM - eg. Standard_A1_v2"
}

variable "scaleset_capacity" {
  description = "Number of VMs in the scale set"
}

variable "subnet_id" {
  description = "The subnet required"
}

variable "net_secgrp" {
  description = "The network security group webservers should be placed in"
}

variable admin_username {
  description = "Set the default administrator username"
  default     = "f5admin"
}

variable admin_password {
  description = "Set the default administrator password"
}

variable vm_name_prefix {
  description = "Set the prefix for VMs deployed"
}

