output "admin_password" {
  value = module.bigip.random_password_generated
}

output "username" {
  value = var.default_admin
}

output "mgmt_url" {
  value = "https://${module.bigip.azurerm_public_ip_bigipvm}:8443/"
}

output "consul_ui" {
  value = "http://${module.consul.azurerm_public_ip_consul}:8500/"
}

output "app_url" {
  value = "http://${module.bigip.azurerm_public_ip_bigipvm}:8080/"
}
