resource "null_resource" "virtualserverAS3" {
  provisioner "local-exec" {
    command = <<-EOT
        sleep 120 
        curl -s -k -X POST https://${var.public_ip_address}:8443/mgmt/shared/appsvcs/declare \
              -H 'Content-Type: application/json' \
              --max-time 600 \
              --retry 10 \
              --retry-delay 30 \
              --retry-max-time 600 \
              --retry-connrefused \
              -u "admin:${var.admin_password}" \
              -d '${data.template_file.virtualserverAS3.rendered}'
        EOT
  }

  depends_on = [
    var.dependson_linux_vm_f5bigip,
  ]

  triggers = {
    as3_content = data.template_file.virtualserverAS3.rendered
  }
}

data "template_file" "virtualserverAS3" {
  template = file("${path.root}/templates/web.json")
  vars = {
    vip_address = var.vip_address
  }
}

