resource "azurerm_network_interface" "consul-nic" {
  name                = "${var.name_prefix}-consul-nic"
  location            = var.location
  resource_group_name = var.rg_name

  ip_configuration {
    name                          = "${var.name_prefix}-ipconfig"
    subnet_id                     = var.consul_subnet_id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.priv_ip
    public_ip_address_id          = azurerm_public_ip.consul-pip.id
  }
}

resource "azurerm_network_interface_security_group_association" "consul-nic-security" {
  network_interface_id      = azurerm_network_interface.consul-nic.id
  network_security_group_id = var.netsec_group_id
}


resource "azurerm_public_ip" "consul-pip" {
  name                = "${var.name_prefix}-ip"
  location            = var.location
  resource_group_name = var.rg_name
  allocation_method   = "Dynamic"
  domain_name_label   = "${var.name_prefix}-consul"
}

resource "azurerm_linux_virtual_machine" "consul" {
  name                  = "consul-server"
  computer_name         = var.name_prefix
  admin_username        = var.admin_username
  admin_password        = var.admin_password
  location              = var.location
  resource_group_name   = var.rg_name
  size                  = "Standard_A0"
  custom_data           = base64encode(file("${path.root}/templates/consul.sh"))
  network_interface_ids = [azurerm_network_interface.consul-nic.id]

  disable_password_authentication = false
  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  os_disk {
    storage_account_type = "Standard_LRS"
    caching              = "ReadWrite"
  }
}
