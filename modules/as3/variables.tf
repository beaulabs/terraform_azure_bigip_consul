variable "vip_address" {
  description = "Virtual IP address of AS3"
}

# Do to modules not being able to support depends_on directly yet
# we will emulate using a variable to pass through the depends_on 
# capability
variable "dependson_linux_vm_f5bigip" {
  description = "F5 BigIP must be up and running first on a Linux VM"
  type        = any
  default     = null
}

# Do to modules not being able to support depends_on directly yet
# we will emulate using a variable to pass through the depends_on 
# capability ....THIS WAS DEPRECATED, but left for reference
variable "dependson_linux_vm_extension_startup_cmd" {
  description = "The start-up command script must be run on the Linux VM"
  type        = any
  default     = null
}

variable "public_ip_address" {
  description = "The public IP address to use for the AS3 configuration"
}

variable "admin_password" {
  description = "Password to use for AS3"
}
