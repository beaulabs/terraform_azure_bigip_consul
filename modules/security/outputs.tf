output "webserver_security_group_id" {
  description = "The webserver security group id"
  value       = azurerm_network_security_group.webserver-sg.id
}

output "consul_security_group_id" {
  description = "The consul security group id"
  value       = azurerm_network_security_group.consul-sg.id
}

output "f5_public_security_group_id" {
  description = "The F5 public security group id"
  value       = azurerm_network_security_group.f5_public.id
}
